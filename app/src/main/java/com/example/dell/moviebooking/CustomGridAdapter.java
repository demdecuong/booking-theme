package com.example.dell.moviebooking;

import android.content.Context;
import android.graphics.Color;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class CustomGridAdapter extends BaseAdapter {
    private ArrayList<Seat> listData;
    // private LayoutInflater layoutInflater;
    private Context context;

    public CustomGridAdapter(Context context, ArrayList<Seat> listData) {
        this.context = context;
        this.listData = listData;
    }


    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
            if(convertView == null)
            {
                convertView = LayoutInflater.from(context).inflate(R.layout.gridview_item,null);
                viewHolder = new ViewHolder();
                viewHolder.imgSeat = convertView.findViewById(R.id.imageViewSeat);
                convertView.setTag(viewHolder);
            }else
                viewHolder = (ViewHolder)convertView.getTag();

            // CHECK MODE TO CHECK COLOR
            int mode  = listData.get(position).getMode();
            //WHITE
        if( mode == -1)
        {
             viewHolder.imgSeat.setImageResource(R.drawable.cinema_seat_empty);
        }
        else
        if( mode == 0)
            viewHolder.imgSeat.setImageResource(R.drawable.cinema_seat);
        else if(mode ==1)
            viewHolder.imgSeat.setImageResource(R.drawable.cinema_seat_booked);
        else
            viewHolder.imgSeat.setImageResource(R.drawable.cinema_seat_selected);


        return convertView;
}

    static class ViewHolder {
        ImageView imgSeat;
     }
}
