package com.example.dell.moviebooking;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
{
    private static int selectedPosition = 0;
    private Context context;
    private ArrayList<Date> data;
    LayoutInflater inflater;
    private static OnItemClickListener listener;
    RecyclerViewVerticalAdapter recyclerViewVerticalAdapter;

    public RecyclerViewAdapter(Context context, ArrayList<Date> arrayList, RecyclerViewVerticalAdapter recyclerViewVerticalAdapter) {
        this.context=context;
        this.data=arrayList;
        this.recyclerViewVerticalAdapter = recyclerViewVerticalAdapter;
    }


    public String getCurrentDate()
    {
        return String.valueOf(data.get(selectedPosition).Date);
    }

    public String getCurrentDay() {
        return String.valueOf(data.get(selectedPosition).Day);
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public RecyclerViewAdapter(Context context, ArrayList<Date> arrayList) {
        this.data = arrayList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.date_item, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final Date item = data.get(i);
        String name = String.valueOf(data.get(i).getDay());
        viewHolder.txtDay.setText(name);
        name = String.valueOf(data.get(i).getDate());
        viewHolder.txtDate.setText(name);
        viewHolder.linearLayout.setBackgroundResource(R.drawable.borderbluebutton);

        if(selectedPosition==i)
            viewHolder.itemView.setBackgroundResource(R.drawable.borderbluebutton);
        else
            viewHolder.itemView.setBackgroundResource(R.drawable.borderbutton);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition=i;
                notifyDataSetChanged(); //notify to redrawn itself
                recyclerViewVerticalAdapter.selectedRow = 0;
                recyclerViewVerticalAdapter.selectedItem = 0;
                recyclerViewVerticalAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDay,txtDate;
        public LinearLayout linearLayout;

        public ViewHolder(final View itemView) {
            super(itemView);
            txtDay = (TextView) itemView.findViewById(R.id.textViewDay);
            txtDate = (TextView) itemView.findViewById(R.id.textViewDate);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.LinearLayoutDate);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }
}