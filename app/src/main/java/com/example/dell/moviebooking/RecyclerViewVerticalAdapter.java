package com.example.dell.moviebooking;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class RecyclerViewVerticalAdapter extends RecyclerView.Adapter<RecyclerViewVerticalAdapter.VerticalViewHolder> {
    Context context;
    ArrayList<VerticalModel>arrayList;
    List<RecyclerView> items;
    HorizontalRecyclerViewAdapter horizontalRecyclerViewAdapter;
    static int selectedRow=0;
    static int selectedItem=0;
    static int flag  = 1;

    public RecyclerViewVerticalAdapter(MainActivity mainActivity, ArrayList<VerticalModel> verticalModelArrayList, int selectedRow, int selectedItem) {
        this.context = context;
        this.arrayList = verticalModelArrayList;
        this.selectedItem= selectedItem;
        this.selectedRow = selectedRow;
    }

    public HorizontalRecyclerViewAdapter getHorizontalRecyclerViewAdapter() {
        return horizontalRecyclerViewAdapter;
    }

    public RecyclerViewVerticalAdapter(Context context, ArrayList<VerticalModel> verticalModelArrayList) {
        this.context = context;
        this.arrayList = verticalModelArrayList;
    }

    @NonNull
    @Override
    public VerticalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cinema_item,viewGroup,false);
        Log.d("VERTICAL CREATE","--------------------------------");
        return new VerticalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VerticalViewHolder verticalViewHolder, final int i) {
        VerticalModel verticalModel = arrayList.get(i);
        verticalViewHolder.txtCinemaName.setText(verticalModel.getName());
        ArrayList<HorizontalModel> singleItem = verticalModel.getArrayList();

        // Remain the state of horizontaladapter state
        horizontalRecyclerViewAdapter = new HorizontalRecyclerViewAdapter(context, singleItem,
                this,verticalViewHolder,selectedRow,selectedItem);

        verticalViewHolder.recyclerViewHorizontal.setHasFixedSize(true);
        verticalViewHolder.recyclerViewHorizontal.setLayoutManager(new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL,false));
        verticalViewHolder.recyclerViewHorizontal.setAdapter(horizontalRecyclerViewAdapter);
    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class VerticalViewHolder extends  RecyclerView.ViewHolder{
        TextView txtCinemaName;
        RecyclerView recyclerViewHorizontal;

        public VerticalViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCinemaName = itemView.findViewById(R.id.textViewCinemaName);
            recyclerViewHorizontal = itemView.findViewById(R.id.recycleViewHorizontal);
        }
    }
}
