package com.example.dell.moviebooking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TotalSeat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_seat);
        Intent intent = getIntent();
        String strTotalSeat = intent.getStringExtra("totalSeat");
        strTotalSeat = "X "+strTotalSeat;
        Intent returnedIntent = new Intent();
        returnedIntent.putExtra("result",strTotalSeat);
        setResult(RESULT_OK, returnedIntent);
        finish();

    }
}
