package com.example.dell.moviebooking;

import android.content.Context;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Random;


import java.util.ArrayList;

class HorizontalRecyclerViewAdapter extends RecyclerView.Adapter<HorizontalRecyclerViewAdapter.HorizontalViewHolder>{

    public HorizontalRecyclerViewAdapter(Context context, ArrayList<HorizontalModel> singleItem, RecyclerViewVerticalAdapter recyclerViewVerticalAdapter, RecyclerViewVerticalAdapter.VerticalViewHolder verticalViewHolder, int selectedRow, int selectedItem) {
        this.context=context;
        this.arrayList = singleItem;
        this.recyclerViewVerticalAdapter = recyclerViewVerticalAdapter;
        this.verticalViewHolder = verticalViewHolder;
        this.selectedPosition = selectedItem;
        this.row = selectedRow;
        Log.d("SELECTED ITEM = ", String.valueOf(selectedItem));
    }

    int selectedPosition ;
    Context context;
    ArrayList<HorizontalModel> arrayList;
    int flag = 1;
    Random rand = new Random();
    //int a[]={rand.nextInt(4)};
    int a[]={1};
    RecyclerViewVerticalAdapter recyclerViewVerticalAdapter;
    RecyclerViewVerticalAdapter.VerticalViewHolder verticalViewHolder;
    int rowCopy;
    static int row = 0;  //why k change into 2 ???
    static int selectedPositionCopy = 0;



    @NonNull
    @Override
    public HorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cinemahour_item,viewGroup,false);
     //   selectedPosition = selectedPositionCopy;
        return new HorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HorizontalViewHolder horizontalViewHolder, final int i) {
        final HorizontalModel horizontalModel = arrayList.get(i);
        horizontalViewHolder.txtHour.setText(horizontalModel.getHour());
//        items.add(horizontalViewHolder.linearLayoutBackGround);
    Log.d("I = ", String.valueOf(i));
    Log.d("SelectedPos", String.valueOf(selectedPosition));
        if(selectedPosition == i && i != a[0] && row == verticalViewHolder.getAdapterPosition()) {
            horizontalViewHolder.itemView.setBackgroundResource(R.drawable.borderbluebutton);
        }
        else {
            if(a[0] != i) {
                horizontalViewHolder.itemView.setBackgroundResource(R.drawable.borderbutton);

            }else
                horizontalViewHolder.itemView.setBackgroundResource(R.drawable.borderredbutton);
            }
       horizontalViewHolder.txtHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(row!=verticalViewHolder.getAdapterPosition())
                    recyclerViewVerticalAdapter.notifyItemChanged(row);
                 if(selectedPosition==i)
                    {
                        horizontalViewHolder.itemView.setBackgroundResource(R.drawable.borderbluebutton);
                        selectedPosition = i;
                    }else
                        {
                        if (a[0] != i) {
                            horizontalViewHolder.itemView.setBackgroundResource(R.drawable.borderbutton);
                            selectedPosition = i;
                        }else
                            horizontalViewHolder.itemView.setBackgroundResource(R.drawable.borderredbutton);
                    }
                row = verticalViewHolder.getAdapterPosition();
                rowCopy = row;
                selectedPositionCopy = selectedPosition;
                //SET STATIC VALUE
                recyclerViewVerticalAdapter.selectedRow = row;
                recyclerViewVerticalAdapter.selectedItem = selectedPositionCopy;
                //Toast.makeText(context, arrayList.get(i).getHour(), Toast.LENGTH_SHORT).show();
                horizontalViewHolder.itemView.setBackgroundResource(R.drawable.borderbluebutton);
                notifyDataSetChanged(); //notifyiem
            }
        });
    }
    @Override
    public int getItemCount() {
       return arrayList.size();
    }

    public class HorizontalViewHolder extends RecyclerView.ViewHolder{
        TextView txtHour;
        LinearLayout linearLayoutBackGround;
        public HorizontalViewHolder(@NonNull View itemView) {
            super(itemView);
            txtHour = itemView.findViewById(R.id.textViewCinemaHour);
            linearLayoutBackGround = itemView.findViewById(R.id.LinearLayoutCinemaHour);
        }
    }
}
