package com.example.dell.moviebooking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;

public class Date implements Serializable {
    public String getDay() {
        return Day;
    }

    public int getDate() {
        return Date;
    }

    public String[] getDayArr() {
        return dayArr;
    }

    String Day;
    int Date;
    String[] dayArr = {"","","Mon","Tue","Wed","Thu","Fri","Sat","Sun"};

    public Date(int date, int day) {
        super();
        Date= date;
        Day = dayArr[day];
    }

    public View createAndBind2ItemView(Context context) {
        View dateView = LayoutInflater.from(context).inflate(R.layout.date_item,null);

        TextView textTitle = (TextView) dateView.findViewById(R.id.textViewDay);
        textTitle.setText(Day);
        TextView textWebsite = (TextView) dateView.findViewById(R.id.textViewDate);
        textWebsite.setText(Day.toString());

        dateView.setTag(this); //set view as an object
        return dateView;
    }
}
