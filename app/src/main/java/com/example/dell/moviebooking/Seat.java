package com.example.dell.moviebooking;

class Seat {
    int mode;  //mode = 0:available,1:booked,2:selected
    Boolean isSelected = false;

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public Seat(int randMode) {
        this.mode=randMode;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
}
