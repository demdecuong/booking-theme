package com.example.dell.moviebooking;

import java.util.ArrayList;

class VerticalModel {
    String Name;
    ArrayList<HorizontalModel>arrayList;
    public void setName(String name) {
        Name = name;
    }

    public void setArrayList(ArrayList<HorizontalModel> arrayList) {
        this.arrayList = arrayList;
    }

    public String getName() {
        return Name;
    }

    public ArrayList<HorizontalModel> getArrayList() {
        return arrayList;
    }

}
