package com.example.dell.moviebooking;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class booking extends AppCompatActivity {
    public static final int REQUEST_CODE_GETTOTALSEAT = 2804;
    FloatingActionButton floatingActionButton;
    int selectedRow;
    int selectedItem;
    int totalSeat = 0;
    int arr[] = {0,1,3,8,10,11,
            12,15,20,23,
            27,32,
            39,44,
            51,56,
            56+7,56+7+5,
            56+7+5+7-3,56+7+5+7-2,56+7+5+7-1,56+7+5+7,56+7+5+7+1,56+7+5+7+2,56+7+5+7+3,56+7+5+7+4,56+7+5+7+5,56+7+5+7+5+1,56+7+5+7+5+2,56+7+5+7+5+3,
            56+7+5+7+5+7,56+7+5+7+5+7+5,
            56+7+5+7+5+7+5+7,56+7+5+7+5+7+5+7+5};

    int selectedSeat[] = new int [600];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_booking);
        Intent intent = getIntent();
        init(intent);
        floatingActionButton = findViewById(R.id.floatingbutton2);
        final TextView textViewTotalSeat = findViewById(R.id.textViewTotalSeat);

        final Intent intentMain = new Intent(this, MainActivity.class);

        final GridView gridView = (GridView)findViewById(R.id.gridView);

        //Generate Data
        final ArrayList<Seat> listData = new ArrayList<>();
        initListData(listData);
        Arrays.fill(selectedSeat,0);
        final CustomGridAdapter customGridAdapter = new CustomGridAdapter(this, listData);
        gridView.setAdapter(customGridAdapter );

        final Intent getTotalSeatIntent = new Intent(this, TotalSeat.class);
       // MUST SET YOUR ADPTER BE FINAL !!! and notifydatachanged
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Seat seat = listData.get(position);

                if(selectedSeat[position]==0) {
                    if (canSelected(position,listData)) {
                        selectedSeat[position] = 1;
                        totalSeat += 1;
                        seat.setMode(2);
                    }
                }else
                {
                    selectedSeat[position]=0;
                    totalSeat -= 1;
                    seat.setMode(0);
                }
                customGridAdapter.notifyDataSetChanged(); //
                String tmp = String.valueOf(totalSeat);
                getTotalSeatIntent.putExtra("totalSeat",tmp);
                startActivityForResult(getTotalSeatIntent, REQUEST_CODE_GETTOTALSEAT);
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentMain.putExtra("selectedRow",selectedRow);
                intentMain.putExtra("selectedItem",selectedItem);

                startActivity(intentMain);
            }
        });


    }

    private boolean canSelected(int position,ArrayList<Seat> listData) {
        if(listData.get(position).getMode()==-1 || listData.get(position).getMode() ==1)
            return false;
        return true;
    }


    private void setClickEvent(final GridView gridView, final TextView textViewTotalSeat) {
       for(int i=0;i<gridView.getChildCount();i++)
       {
           final ImageView item = (ImageView) gridView.getChildAt(i);

           final int finalI = i;
           item.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   if(hasBeenSelected(selectedSeat, finalI)==false)
                   {
                       item.setBackgroundResource(R.drawable.cinema_seat_selected);
                       selectedSeat[finalI]=1;
                       totalSeat+=1;
                   }else
                   {
                       item.setBackgroundResource(R.drawable.cinema_seat);
                       selectedSeat[finalI]=0;
                       totalSeat-=1;
                   }
                   Log.d("TOTAL SEAT = ", String.valueOf(totalSeat));
                   Toast.makeText(booking.this, finalI, Toast.LENGTH_SHORT).show();
                   textViewTotalSeat.setText(totalSeat);
               }
           });
       }

    }

    private void initListData(ArrayList<Seat> listData) {
        Random rand = new Random();
        int randMode ;
        Seat singleItem;
        for(int i =0;i<12*9;i++) {

            if(binarySearch(arr,0,arr.length-1,i) == true && i <= arr[arr.length-1])
            {
                singleItem = new Seat(-1);
            }
            else {
                randMode = rand.nextInt(2);
                singleItem = new Seat(randMode);
            }

            listData.add(singleItem);
        }
    }

    private void init(Intent intent) {
        String Date = intent.getStringExtra("Date");
        String Day = intent.getStringExtra("Day");
        String Time = intent.getStringExtra("Time");
        String Cinema = intent.getStringExtra("Cinema");
        TextView textViewDate = findViewById(R.id.textViewDate2);
        textViewDate.setText(Day+","+Date);
        TextView textViewTime = findViewById(R.id.textViewTime2);
        textViewTime.setText(Time);
        TextView textViewCinema = findViewById(R.id.textViewCinema2);
        textViewCinema.setText(Cinema);

        selectedItem = intent.getIntExtra("selectedItem",0);
        selectedRow = intent.getIntExtra("selectedRow",0);
    }
    Boolean binarySearch(int arr[],int l,int r,int x)
    {
        int mid = 0;
        while(l <= r) {
            mid = l + (r - l) / 2;
            if (arr[mid] == x) return true;
            if (arr[mid] < x)
                l = mid + 1;
            else r = mid - 1;
        }
        return  false;
    }
    Boolean hasBeenSelected(int arr[],int x)
    {
        if(arr[x]==1)
            return true;
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode)
        {
            case REQUEST_CODE_GETTOTALSEAT:
                if(resultCode == Activity.RESULT_OK)
                {
                    String strTotalSeat = data.getStringExtra("result" );
                    TextView textView = (TextView)findViewById(R.id.textViewTotalSeat);
                    textView.setText(strTotalSeat);
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
