package com.example.dell.moviebooking;

class HorizontalModel {
    String hour;

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getHour() {
        return hour;
    }
}
