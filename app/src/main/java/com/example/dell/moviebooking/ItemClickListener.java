package com.example.dell.moviebooking;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
}
