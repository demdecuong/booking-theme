package com.example.dell.moviebooking;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    static int selectedRow= 0 ;
    static int selectedItem=0;
    RecyclerView recyclerView;
    RecyclerView  recyclerViewVertical;
    FloatingActionButton floatingActionButton;
    HorizontalRecyclerViewAdapter horizontalRecyclerViewAdapter;
    RecyclerViewVerticalAdapter recyclerViewVerticalAdapter;
    RecyclerViewAdapter recyclerViewAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_main);
        Intent intentBooking = getIntent();
        selectedItem = intentBooking.getIntExtra("selectedItem",-1);
        selectedRow = intentBooking.getIntExtra("selectedRow",-1);

        //Mapping view
        floatingActionButton = findViewById(R.id.floatingbutton);
        recyclerView = (RecyclerView) findViewById(R.id.recycleViewDate);
        recyclerViewVertical = findViewById(R.id.recycleViewVertical);
        recyclerViewVertical.setHasFixedSize(true);

        //Declare data
        ArrayList<Date> arrayList = new ArrayList<>();
        ArrayList<VerticalModel> verticalModelArrayList = new ArrayList<>();

        //Generate adapter
        recyclerViewVerticalAdapter = new RecyclerViewVerticalAdapter(this, verticalModelArrayList,selectedRow,selectedItem);
        recyclerViewAdapter = new RecyclerViewAdapter(this, arrayList,recyclerViewVerticalAdapter);


        //set Layoutmanager
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this,
                LinearLayoutManager.HORIZONTAL, false));
        recyclerViewVertical.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        //
        //set Adapter
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewVertical.setAdapter(recyclerViewVerticalAdapter);

        dateInit(arrayList);
        SetCinemaHour(verticalModelArrayList);


        final Intent intent = new Intent(this, booking.class);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("Date",recyclerViewAdapter.getCurrentDate());
                intent.putExtra("Day",recyclerViewAdapter.getCurrentDay());
                intent.putExtra("Cinema",recyclerViewVerticalAdapter.arrayList
                        .get(recyclerViewVerticalAdapter.selectedRow).getName());
                intent.putExtra("Time",recyclerViewVerticalAdapter.arrayList.get(recyclerViewVerticalAdapter.selectedRow)
                        .getArrayList().get(recyclerViewVerticalAdapter.selectedItem).getHour());
                intent.putExtra("selectedRow",recyclerViewVerticalAdapter.selectedRow);
                intent.putExtra("selectedItem",recyclerViewVerticalAdapter.selectedItem);
                startActivity(intent);
            }
        });
       if(savedInstanceState != null)
        {
            recyclerViewVerticalAdapter.selectedItem = savedInstanceState.getInt("horizontal_pos");
            recyclerViewVerticalAdapter.selectedRow =  savedInstanceState.getInt("vertical_pos");}
      }

   @Override
   protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("vertical_pos",recyclerViewVerticalAdapter.selectedRow);
        Log.d("VERTICALSAVE = ", String.valueOf(recyclerViewVerticalAdapter.selectedRow));
        outState.putInt("horizontal_pos",recyclerViewVerticalAdapter.selectedItem);
        Log.d("HORIZONTALSAVE = ", String.valueOf(recyclerViewVerticalAdapter.selectedItem));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        selectedItem = savedInstanceState.getInt("horizontal_pos");
        selectedRow =  savedInstanceState.getInt("vertical_pos");
    }

    private void SetCinemaHour(ArrayList<VerticalModel> verticalModelArrayList) {
        String[] hour = {"9:30 AM", "12:30 AM", "3:30 PM", "8:40 PM"};
        String[] cinemaName = {"Sathyam Cinemas", "Escape Cinemas", "Cineplex Movies", "Galaxy Cine"};
        for (int i = 0; i < cinemaName.length; i++) {
            VerticalModel verticalModel = new VerticalModel();
            verticalModel.setName(cinemaName[i]);
            ArrayList<HorizontalModel> horizontalModelArrayList = new ArrayList<>();
            for (int j = 0; j < hour.length; j++) {
                HorizontalModel horizontalModel = new HorizontalModel();
                horizontalModel.setHour(hour[j]);

                horizontalModelArrayList.add(horizontalModel);
            }
            verticalModel.setArrayList(horizontalModelArrayList);
            verticalModelArrayList.add(verticalModel);
        }
    }

    private void dateInit(ArrayList<Date> arrayList) {
        arrayList.add(new Date(12, 6));
        arrayList.add(new Date(13, 7));
        arrayList.add(new Date(14, 8));
        arrayList.add(new Date(15, 2));
        arrayList.add(new Date(16, 3));
    }

}
